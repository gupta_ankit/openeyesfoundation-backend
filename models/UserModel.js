const mongoose = require ('mongoose'),
    passwordHash = require('password-hash'),
    Schema = mongoose.Schema,
    UserSchema = new Schema ({
        email: {
            required: [true, 'Email required!'],
            type: String,
            lowercase: true,
            trim: true,
            maxlength: 255,
            unique: [true, 'Email already registered!'],
            validate: {
                validator: function (email) {
                    return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test (email);
                },
                message: '{VALUE} is not a valid Email!'
            }
        }, 
        password: {
            required: [true, 'Password required!'],
            type: String,
            trim: true,
            // maxlength:50,
            set: hashPassword
        },
        role: {
            trim: true,
            required: true,
            type: String,
            enum: ['admin', 'user', 'volunteer'],
            lowercase: true,
            default: 'user'
        },
        is_active: {
            type: Boolean,
            default: true
        },
        profile: [{
            type: Schema.Types.ObjectId, 
            ref: 'profile' 
        }],
        created_at: {
            type: Date, 
            default: Date.now
        },
        updated_at: {
            type: Date,
            default: Date.now
        },
        // deleted_at: {
        //     type: Date,
        //     default: null
        // }
    });

    // set user  role before save
    // first user would be admin
    // else user role will be as user
    UserSchema.pre('save', function(next) {
        var self = this;
        self.constructor.count(function(err, data) {
            if(err)
                return next(err);
            // if no error do something as you need and return callback next() without error
            if (data == 0)
                self.set ('role', 'admin');
            return next();
        });
    });

    // save update time before update
    UserSchema.pre('update', function(next) {
        self.set ('updated_at', Date.now);
        return next();
    });

    // save deleted time before delete
    // UserSchema.pre('delete', function(next) {
    //     self.set ('deleted_at', Date.now);
    //     return next();
    // });

function hashPassword (password) {
    return passwordHash.generate(password);
}

module.exports = mongoose.model('users', UserSchema);