const mongoose = require ('mongoose'),
    Schema = mongoose.Schema,
    today = new Date;
    BookSchema = new Schema ({
        title: {
            type: String,
            required: [true, 'Book Title is required'],
            trim: true,
            lowercase: true,
            maxlength: 255
        },
        description: {
            type: String,
            trim: true,
            lowercase: true,
            maxlength: 2000,
            default: null
        },
        author_name: {
            type: String,
            trim: true,
            lowercase: true,
            maxlength: 255,
            default: null
        },
        publication_name: {
            type: String,
            trim: true,
            lowercase: true,
            maxlength: 255,
            default: null
        },
        year_of_printing: {
            type: Number,
            trim: true,
            lowercase: true,
            min: 1920,
            max: today.getFullYear(),
            default: null
        },
        front_cover_image: {
            type: String,
            trim: true,
            default: null
        },
        back_cover_image: {
            type: String,
            trim: true,
            default: null
        },
        condition: {
            type: String,
            enum: {
                values: ['new', 'very good', 'good', 'ok', 'bad', 'very bad', 'poor'],
                message: 'Condition must be one of the value: new, very good, good, ok, bad, very bad, poor'
            },
            required: [true, 'Book condition is required'],
        },
        number_of_book: {
            type: Number,
            default:1
        },
        created_at: {
            type: Date, 
            default: Date.now
        },
        updated_at: {
            type: Date,
            default: Date.now
        },
        // deleted_at: {
        //     type: Date,
        //     default: null
        // }
    });

    // save update time before update
    BookSchema.pre('update', function(next) {
        self.set ('updated_at', Date.now);
        return next();
    });

    // save deleted time before delete
    // BookSchema.pre('delete', function(next) {
    //     self.set ('deleted_at', Date.now);
    //     return next();
    // });

module.exports = mongoose.model('book', BookSchema);