const mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    StudentSchema = new Schema({
        name: {
            type: String,
            required: true,
            trim: true,
            lowercase: true,
            maxlength: 255
        },
        father_name: {
            type: String,
            trim: true,
            lowercase: true,
            maxlength: 255,
            default: null
        },
        mother_name: {
            type: String,
            trim: true,
            lowercase: true,
            maxlength: 255,
            default: null
        },
        dob: {
            type: Date,
            trim: true,
            default: null
        },
        address: {
            type: String,
            trim: true,
            lowercase: true,
            maxlength: 255,
            default: null
        },
        school_name: {
            type: String,
            trim: true,
            lowercase: true,
            maxlength: 255,
            default: null
        },
        class: {
            type: String,
            trim: true,
            lowercase: true,
            maxlength: 255,
            default: null
        },
        profile_image: {
            type: String,
            trim: true,
            lowercase: true,
            maxlength: 255,
            default: null
        },
        created_at: {
            type: Date,
            default: Date.now
        },
        updated_at: {
            type: Date,
            default: Date.now
        },
        // deleted_at: {
        //     type: Date,
        //     default: null
        // }
    });

// save update time before update
UserSchema.pre('update', function (next) {
    self.set('updated_at', Date.now);
    return next();
});

// save deleted time before delete
// UserSchema.pre('delete', function (next) {
//     self.set('deleted_at', Date.now);
//     return next();
// });

module.exports = mongoose.model('student', StudentSchema);