const mongoose = require ('mongoose'),
    Schema = mongoose.Schema,
    today = new Date;
    PaymentSchema = new Schema ({
        name: {
            type: String,
            trim: true,
            lowercase: true,
            maxlength: 255
        },
        email: {
            type: String,
            trim: true,
            lowercase: true,
            maxlength: 255
        },
        phone: {
            type: String,
            trim: true,
            lowercase: true,
            maxlength: 255
        },
        amount: {
            type: String,
            trim: true,
            lowercase: true,
            maxlength: 255
        },
        status: {
            type: Boolean
        },
        payment_response: {
            type: JSON,
            trim: true,
        },
        created_at: {
            type: Date, 
            default: Date.now
        },
        updated_at: {
            type: Date,
            default: Date.now
        },
    });

    // save update time before update
    PaymentSchema.pre('update', function(next) {
        self.set ('updated_at', Date.now);
        return next();
    });

module.exports = mongoose.model('payment', PaymentSchema);