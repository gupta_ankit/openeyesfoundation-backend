const mongoose = require ('mongoose'),
Schema = mongoose.Schema,
ProfileSchema = new Schema ({
    first_name: {
        type: String,
        trim: true,
        maxlength: 255,
        default: null
    },
    last_name: {
        type: String,
        trim: true,
        maxlength: 255,
        default: null
    },
    gender: {
        type: String,
        trim: true,
        enum: ['male', 'female', 'other'],
        default: 'male'
    },
    phone: {
        type: String,
        trim: true,
        maxlength: 10,
        default: '0000000000',
        validate: {
            validator: function (email) {
                return /[\d]{10}/.test (email);
            },
            message: '{VALUE} is not a valid Phone!'
        }
    },
    dob: {
        type: Date,
        default: null
    },
    address: {
        type: String,
        trim: true,
        maxlength: 255,
        default: null
    },
    user_id: [{
        type: Schema.Types.ObjectId, 
        ref: 'users' 
    }],
    created_at: {
        type: Date, 
        default: Date.now
    },
    updated_at: {
        type: Date,
        default: Date.now
    },
    // deleted_at: {
    //     type: Date,
    //     default: null
    // }
});

// save update time before update
ProfileSchema.pre('update', function(next) {
    self.set ('updated_at', Date.now);
    return next();
});

// ProfileSchema.post('findOneAndUpdate', function(data) {
//     // console.log (mongoose.model('users'));
//     // self.set ('updated_at', Date.now);
//     mongoose.model('users').findById (data.user_id[0], {$set: {profile: data._id}}, (err, user) => {
//         if (err) 
//             return console.log (err);
//         console.log (user);
//     });
//     // return next();
// });

// save deleted time before delete
// ProfileSchema.pre('delete', function(next) {
//     self.set ('deleted_at', Date.now);
//     return next();
// });

module.exports = mongoose.model('profile', ProfileSchema);