let authmiddleware = require ('./authmiddleware'),
    adminmiddleware = require('./adminmiddleware');
    paymentmiddleware = require('./paymentmiddleware');

module.exports = {
    // Check if token exists in header
    checktoken: authmiddleware.checktoken,

    // check if token is valid
    checktokenvalid: authmiddleware.checktokenvalid,

    // check if user role is admin
    allowadminonly: adminmiddleware.allowadminonly,

    // validate payment request
    validatePaymentrequest: paymentmiddleware.validatePaymentrequest
}