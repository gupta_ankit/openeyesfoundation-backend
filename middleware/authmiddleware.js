const jwt = require('jsonwebtoken'),
    fs = require ('fs');

// verify a token asymmetric
var cert = fs.readFileSync('keys/public.pem');  // get public key



module.exports = {
    /**
     * Check if token exists in header
     */
    checktoken: function (req, res, next) {
        if (!req.headers.token) 
            return res.send (401, {status: false, message: 'Token not found', errors: "Token not found in header"});
        next ();
    },

    /**
     * Check if token is valid or not
     */
    checktokenvalid: function (req, res, next) {
        jwt.verify(req.headers.token, cert, { algorithms: ['RS256'] }, function (err, payload) {
            if (err) 
                return res.send (401, {status: false, message: 'Token Error', errors: err.message});
            
            // set user-information in request
            req.params.user = payload;
            next ();
        });

    }
}