module.exports = {
    /**
     * Allow only admin 
     */
    allowadminonly: function (req, res, next) {
        if (req.params.user.role != 'admin')
            return res.send(403, { status: false, message: 'Permission denied', errors: "You are not authorize to access this service" });
        next();
    }
}