module.exports = {
    /**
     * Method to validate payment request form
     */
    validatePaymentrequest (req, res, next) {
        let errors = {};

        if (!req.body.firstname)
            errors['firstname'] = "firstname can not be empty";
        else if (req.body.firstname && !((/^[a-zA-Z\s]{1,50}$/).test(req.body.firstname)))
            errors['firstname'] = "firstname must be characters upto 50.";

        if (!req.body.phone)
            errors['phone'] = "phone can not be empty";
        else if (req.body.phone && !((/^[0-9]{10,15}$/).test(req.body.phone)))
            errors['phone'] = "phone must be number between 10-15 digits.";

        if (!req.body.email)
            errors['email'] = "email can not be empty";
        else if (req.body.email && !((/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$/).test(req.body.email)))
            errors['email'] = "email must be valid.";

        if (!req.body.amount)
            errors['amount'] = "amount can not be empty";
        else if (req.body.amount && !((/^[0-9]{1,6}$/).test(req.body.amount)))
            errors['amount'] = "amount must be number between 1-6 digits.";

        if (Object.keys(errors).length)
            return res.send(422, { status: false, message: 'Parameter Error', errors: errors });
        
        next();
    }
}