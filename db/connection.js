const mongoose = require ('mongoose');

let options = { auth: { user: "openeyesfoundation", password: "openeyesfoundation", dbName: "openeyesfoundation" } };

// Or using promises
mongoose.connect("mongodb://ds247759.mlab.com:47759", options).then(
    () => { 
      /** ready to use. The `mongoose.connect()` promise resolves to undefined. */ 
      console.log ("Database successfully connected.");
},
  err => { 
      /** handle initial connection error */ 
        console.info (`Database connection error: ${err}`);
    }
);