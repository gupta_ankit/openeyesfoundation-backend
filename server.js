const restify = require ('restify'),
    connection = require ('./db/connection'),
    fs = require ('fs'),
    corsMiddleware = require ('restify-cors-middleware'),
    path = require ('path'),
    port = process.env.port || 3000;

let https_options = {
    key: fs.readFileSync('/home/ankit/dev.openeyesfoundation.org.key'),
    certificate: fs.readFileSync('/home/ankit/dev.openeyesfoundation.org.crt')
};

const server = restify.createServer (https_options);

// generate key-pair if not exists
// check if user-authorization key file exists
if (!fs.existsSync(path.join(__dirname, 'keys', 'private.key')) || !fs.existsSync(path.join(__dirname, 'keys', 'public.pem'))) {

    fs.mkdirSync('keys');

    // generate secret key for user authorization
    const keypair = require('keypair');

    var pair = keypair({
        bits: 2048, // size for the private key in bits. Default: 2048
        e: 65537 // public exponent to use. Default: 65537
    });

    fs.writeFile(__dirname + '/keys/private.key', pair.private, function (err) {
        if (err) {
            return console.error(err);
        }
        console.log('RSA private key file generated');
    });

    fs.writeFile(__dirname + '/keys/public.pem', pair.public, function (err) {
        if (err) {
            return console.error(err);
        }
        console.log('RSA public key file generated');
    });
}

const cors = corsMiddleware({
    origins: ['https://localhost:4200'],
    allowHeaders: ['X-App-Version'],
    exposeHeaders:[]
});

server.pre (cors.preflight);
server.use (cors.actual);

// cors middleware
// server.on('MethodNotAllowed', require(path.join(__dirname, 'middleware', 'corsmiddleware')));


server.use(restify.plugins.bodyParser());
server.use(restify.plugins.queryParser());
server.pre(restify.plugins.inflightRequestThrottle({ limit: 600, server: server }));

// server each request
fs.readdirSync ('./controllers').forEach (function (file) {
    require('./controllers/' + file).controller (server);
})

server.listen (port, () => {
    console.info (`Server running on port ${port}`)
});