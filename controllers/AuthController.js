const UserModel = require ('../models/UserModel'),
    passwordHash = require('password-hash'),
    fs = require ('fs'),
    jwt = require('jsonwebtoken'),      // sign with default (HMAC SHA256)

    // sign with RSA SHA256
    private_cert = fs.readFileSync('keys/private.key');  // get private key

module.exports.controller = (server) => {
    /**
     * Home Page
     */
    server.post ('/register', (req, res) => {
        let user = new UserModel (req.params);

        // save user-information
        user.save((err, data) => {
            if (err)
                return res.send(500, {status: false, message: "Unable to register user", errors: err.message});

            return res.send(201, { status: true, message: 'User successfully registered', data: { user_id: data._id}});
        });
    })

    /**
     * Method to login user
     */
    server.post ('/login', (req, res) => {
        UserModel.findOne({email: req.params.email}, (err, data) => {
            if (err) 
                return res.send(500, { status: false, message: "Unable to login", errors: err.message });

            if (!data)
                return res.send(404, { status: false, message: "User does not exists", errors: "Invalid email"});
            
            // validate user password
            if (!passwordHash.verify(req.params.password, data.password))
                return res.send(403, { status: false, message: "Password mis-match", errors: "Invalid Password"});

            // generate login-token for user
            // sign asynchronously
            jwt.sign({ user_id: data.id, role: data.role, email: data.email }, private_cert, { algorithm: 'RS256', expiresIn : '1d' }, function(err, token) {
                if (err)
                    return res.send(500, { status: false, message: "Token error", errors: err});
                
                return res.send (200, {status:true, message:'User successfully logged-In', errors:{'Token': token}});
            });
        });
    })
}