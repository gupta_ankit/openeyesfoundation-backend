const middleware = require ('../middleware'),
    ProfileModel = require ('../models/ProfileModel'),
    UserModel = require ('../models/UserModel');

module.exports.controller = (server) => {
    /**
     * Save user profile
     */
    server.post('/user', [middleware.checktoken, middleware.checktokenvalid], (req, res) => {
        // save customer profile 
        ProfileModel.findOneAndUpdate ({user_id:req.params.user.user_id}, req.params, {upsert: true, new: true, runValidators: true, setDefaultsOnInsert: true}, (err, data) => {
            if (err)
                return res.send(422, { status: false, message: "Parameters Error", errors: err.message});

            UserModel.findByIdAndUpdate (req.params.user.user_id, {$set: {profile: data._id}}, (err, data) => {
                if (err)
                    return console.log (err);
                console.log ("User profile successfully linked with user");
            });

            return res.send(200, { status: true, message: "Profile successfully saved", data: {} })
        });
    })

    /**
     * Fetch login user information
     */
    server.get('/user', [middleware.checktoken, middleware.checktokenvalid], (req, res) => {
        UserModel.findOne ({_id:req.params.user.user_id})
        .select ('email role created_at')
        .populate ({path: 'profile', select: 'first_name last_name gender phone dob address'})
        .exec(function (err, data) {
            if (err)
                return res.send(422, { status: false, message: "Parameters Error", errors: err.message});
            return res.send(200, { status: true, message: "Profile successfully found", data: data })
        });
    })

    /**
     * Fetch list of all user except admin
     * @role admin only
     */
    server.get('/users', [middleware.checktoken, middleware.checktokenvalid, middleware.allowadminonly], function (req, res) {

        let perPage = 10
            , page = req.query.page ? req.query.page : 0;

        // fetch list of all user excepts admin
        UserModel.find({ role: { $ne: 'admin' } })
            .select('-updated_at -deleted_at -__v -password')
            .limit(perPage)
            .skip(perPage * page)
            .sort({ created_at: 'asc' })
            .populate({ path: 'profile', select: '-updated_at -deleted_at -__v' })
            .exec(function (err, data) {
                if (err)
                    return res.send(500, { status: false, message: 'List of all users', errors: err });

                return res.send(200, { status: true, message: 'List of all users', data: data });
            })
    })
}