const middleware = require ('../middleware'),
BookModel = require ('../models/BookModel');

module.exports.controller = (server) => {

    /**
     * Save book information
     */
    server.post('/book', [middleware.checktoken, middleware.checktokenvalid, middleware.allowadminonly],(req, res) => {
        // save book information
        let Book = new BookModel (req.params);
        Book.save(function (err, data) {
            if (err) 
                return res.send (500, {status: false, message: 'Parameter Errors', errors: err.message});

            return res.send(201, { status: true, message: 'Book information successfully saved', data: {book_id: data._id}});
        })
    })


    /**
     * Fetch list of all books
     */
    server.get('/books', [middleware.checktoken, middleware.checktokenvalid, middleware.allowadminonly], (req, res) => {

        let perPage = 10
            , page = req.query.page ? req.query.page : 0;

        BookModel.find()
        .select ('-updated_at -deleted_at -__v')
        .limit(perPage)
        .skip(perPage * page)
        .sort({ created_at: 'asc' })
        .exec ((err, data) => {
            if (err)
                return res.send(500, { status: false, message: 'Parameter Errors', errors: err });
            return res.send(200, { status: true, message: 'Books list successfully found', data: data });
        })
    })

    /**
     * fetch particular book
     */
    server.get('/book/:id', [middleware.checktoken, middleware.checktokenvalid, middleware.allowadminonly],(req, res) => {
        console.log (req.params.id);
        // Find book information
        BookModel.findById(req.params.id)
            .select('-updated_at -deleted_at -__v')
        .exec(function (err, data) {
            if (err) 
                return res.send (500, {status: false, message: 'Parameter Errors', errors: err});

            return res.send(200, { status: true, message: 'Book information successfully found', data: data});
        })
    })
}