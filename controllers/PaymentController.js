const payumoney_config = require('../config/payumoney'),
    crypto = require('crypto'),
    middleware = require('../middleware'),
    PaymentModel = require ('../models/PaymentModel.js');

module.exports.controller = (server) => {

/**
 * Fetch list of all user except admin
 * @role admin only
 */
    server.post('/payment/make', [middleware.validatePaymentrequest], function (req, res) {
        // Hash Sequence
        let hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";

        let hashVarsSeq = hashSequence.split('|');

        var current_date = (new Date()).valueOf().toString();
        var random = Math.random().toString();
        let random_val = crypto.createHash('md5').update(current_date + random).digest('hex');

        let productinfo = 'Donation';

        // arrangement to generate has code to pass request
        let post_data = {
            key: payumoney_config.MERCHANT_KEY,
            txnid: random_val,
            amount: req.body.amount,
            productinfo: productinfo,
            firstname: req.body.firstname,
            email: req.body.email,
            phone: req.body.phone,
            // surl: req.headers.host + '/' + payumoney_config.SUCCESS_URL,
            // furl: req.headers.host + '/' + payumoney_config.FAILURE_URL,
            surl: 'https://localhost:3000/' + payumoney_config.SUCCESS_URL,
            furl: 'https://localhost:3000/' + payumoney_config.FAILURE_URL,
            service_provider: payumoney_config.SERVICE_PROVIDER
        };

        // generate hash-code of request
        let hash_code = hash_string = '';

        for (hash of hashVarsSeq) {
            hash_string += (hash in post_data) ? post_data[hash] : '';
            hash_string += '|';
        }

        // append salt with hash string
        hash_string += payumoney_config.SALT;

        hash_code = (crypto.createHash('sha512').update(hash_string).digest('hex')).toLowerCase();

        let payment_url = payumoney_config.PAYU_BASE_URL + '/_payment';

        let form = "<form action='" + payment_url +"' method='post' name='payuForm'>";

        for (d in post_data) {
            form += "<input type='hidden' name='" + d + "' value='" + post_data[d] + "' />";
        }
            form += "<input type='hidden' name='hash' value='" + hash_code + "' />";
            form += "<button class='btn btn-outline-success'>Pay Now</button>";
        form += "</form>";

        res.setHeader('Content-Type', 'text/html');
        res.writeHead(200);
        return res.end(form);
    })

    /**
     * Handle payment response
     */
    server.post ('/payment/success', (req, res) => {

        // fetch desired values from payment response received from Pay-U-Money
        let response_payment = {
            name: req.body.firstname,
            email: req.body.email,
            phone: req.body.phone,
            amount: req.body.amount,
            payment_response:req.body,
            status: true
        };

        let Payment = new PaymentModel(response_payment);

        Payment.save(function (err, data) {
            if (err) 
                return res.send (500, {status: false, message: 'Unable to process your donation. We will soon identify your donation status.', errors: err});
            
            let payment_success = "<h1>Payment Success</h1>";
            payment_success += "<table>";
                payment_success += "<tbody>";
                    payment_success += "<tr>";
                        payment_success += "<th>Transaction Number</th>";
                        payment_success += "<td>" + (req.body.txnid).toUpperCase() + "</td>";
                    payment_success += "</tr>";

                    payment_success += "<tr>";
                        payment_success += "<th>Amount</th>";
                        payment_success += "<td>" + req.body.amount + "</td>";
                    payment_success += "</tr>";

                    payment_success += "<tr>";
                        payment_success += "<th>Card Type</th>";
                        payment_success += "<td>" + req.body.bankcode + "</td>";
                    payment_success += "</tr>";

                    payment_success += "<tr>";
                        payment_success += "<th>Card Number</th>";
                        payment_success += "<td>" + req.body.cardnum + "</td>";
                    payment_success += "</tr>";

                    payment_success += "<tr>";
                        payment_success += "<th>Status</th>";
                        payment_success += "<td>" + req.body.status + "</td>";
                    payment_success += "</tr>";

                payment_success += "</tbody>";
            payment_success += "</table>";

            payment_success += "<a href='"+ payumoney_config.PAYMENT_FRONTEND_SUCCESS_URL +"'>Redirect to Website</a>"
                
            res.setHeader('Content-Type', 'text/html');
            res.writeHead(200);
            return res.end(payment_success);
        })
    })

    /**
     * Handle payment response
     */
    server.post ('/payment/failure', (req, res) => {
        return res.send (200, {status:true, message: 'Unable to identify your request. We will soon onform you about your payment status.', data: {}});
    })
}